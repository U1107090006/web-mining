//okuma işlemi
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
//sayma işlemi
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
 

class Main {
  
  public static void main(String[] args) {
    
      String Metin = "";
        //okuma işlemi
        try {
            //dosya açma işlemi
            File f = new File("data.txt");

            BufferedReader b = new BufferedReader(new FileReader(f));

            String readLine = "";

            System.out.println("Reading file using Buffered Reader");
            //satır satır okuma işlemi
            while ((readLine = b.readLine()) != null) {
                System.out.println(readLine);
                Metin += readLine+",";
            }
            // virgülleri boşluk olarak değiştirme
            Metin=Metin.replace(","," ");
            System.out.println(Metin);

        //sayma işlemi
        List<String> list = Arrays.asList(Metin.split(" "));

        Set<String> uniqueWords = new HashSet<String>(list);
        for (String word : uniqueWords) {
            System.out.println(word + ": " + Collections.frequency(list, word));
        }

        } catch (IOException e) {
            e.printStackTrace();
        }


  }
}